#!/bin/bash
#Script de instalaion de jitsi para ubuntu
echo ACTUALIZACION DEL SISTEMA E INSTALACION DE DEPENDENCIAS ;
sleep 3s ;
apt-add-repository universe ;
apt -y update ;
apt -y dist-upgrade ;
apt -y upgrade ;
apt -y install ufw ;
apt -y install virtualenv ;
apt -y install python3-certbot-nginx ;
apt -y install default-jdk ;
apt -y install nginx ;
apt -y install gnupg ;
apt -y install apt-transport-https ;
echo LEVANTANDO NGINX ;
sleep 3s ;
systemctl enable nginx ;
systemctl start nginx ;
echo GENERANDO CLAVES DE LA REPO DE JITSI ;
sleep 3s ;
wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add ;
echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list ;
echo INSTALANDO JITSI ;
sleep 3s ;
apt update ;
apt install jitsi-meet ;
echo CERTIFICANDO ;
sleep 3s
apt install certbot ;
sed -i 's/\.\/certbot-auto/certbot/g' /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh ;
ln -s /usr/bin/certbot /usr/sbin/certbot ;
/usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh ;
echo CONFIGURANDO FIREWALL ;
sleep 3s ;
ufw allow ssh ;
ufw allow http ;
ufw allow https ;
ufw allow in 10000:20000/udp ;
ufw enable ;

