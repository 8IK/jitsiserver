#!/bin/bash
echo GENERANDO CLAVES DE LA REPO DE JITSI ;
sleep 3s ;
wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add ;
echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list ;
echo INSTALANDO JITSI ;
sleep 3s ;
apt update ;
apt install jitsi-meet ;
#Se configuran reglas de firewall ;
echo CONFIGURANDO FIREWALL ;
sleep 3s ;
ufw allow ssh ;
ufw allow http ;
ufw allow https ;
ufw allow in 10000:20000/udp ;
ufw enable ;
